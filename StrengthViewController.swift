//
//  StrengthViewController.swift
//  MyHowYouveGrown
//
//  Created by Huan-Hua Chye on 4/17/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import UIKit
import AVFoundation
import HealthKit

class StrengthViewController: UIViewController {
var seconds = 0
var timer = NSTimer()
var startDate: NSDate?
var endDate: NSDate?
    
    @IBOutlet var homeButton: UIBarButtonItem!
    @IBOutlet weak var strengthImageView: UIImageView!
var timerRunning = false
let speechSynthesizer = AVSpeechSynthesizer()
    
    
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var startStopButton: UIButton!
    
    @IBAction func startStopButtonPressed(sender: AnyObject) {
        if timerRunning {
            //stop
            startStopButton.setTitle(NSLocalizedString("Start!", comment: "command, label for button; when pressed, the count for strength exercises starts"), forState: UIControlState.Normal)
            startStopButton!.titleLabel!.font = UIFont(name: "JACKPORT REGULAR NCV", size: 28)!
            timer.invalidate()
            timerRunning = false
            
            let settings = NSUserDefaults.standardUserDefaults()
            if settings.boolForKey("SFXOn") {

            let speechUtterance = AVSpeechUtterance(string: NSLocalizedString("Great workout. Your monster has been appeased. Total squats: \(seconds).", comment: "phrase spoken upon finishing workout"))
            speechSynthesizer.speakUtterance(speechUtterance)
            }
            
            //MARK: TODO: add workout to user stats: type, monster, duration, start time, end time
            endDate = NSDate()
            let monster = settings.valueForKey("selectedMonster") as? String ?? ""

            HealthKitHelper().writeWorkoutWithMonster(startDate!, endDate: endDate!, workoutType: HKWorkoutActivityType.FunctionalStrengthTraining, monster: monster)
            
        } else {
            //start
            startStopButton.setTitle(NSLocalizedString("Stop!", comment: "command, label for button; when pressed, the count for squats stops"), forState: UIControlState.Normal)
            startStopButton!.titleLabel!.font = UIFont(name: "JACKPORT REGULAR NCV", size: 28)!
            timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(StrengthViewController.addTime), userInfo: nil, repeats: true)
            timerRunning = true
            startDate = NSDate()

        }
    }
    @IBAction func resetCountPressed(sender: UIButton) {
        seconds = 0
        countLabel.text = NSLocalizedString("Squats: ", comment: "label for number of squats completed") + String(seconds)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let settings = NSUserDefaults.standardUserDefaults()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "JACKPORT REGULAR NCV", size: 28)!]
        homeButton.setTitleTextAttributes([ NSFontAttributeName: UIFont(name: "JACKPORT REGULAR NCV", size: 28)!], forState: UIControlState.Normal)


        startStopButton!.titleLabel!.font = UIFont(name: "JACKPORT REGULAR NCV", size: 28)!

        if settings.boolForKey("musicOn") {
            MusicHelper.sharedHelper.playBackgroundMusic("Chariots Of The Elder Gods")
        } else {
            MusicHelper.sharedHelper.stopBackgroundMusic()
        }

        if settings.boolForKey("SFXOn") {

            strengthImageView.contentMode = UIViewContentMode.ScaleAspectFit
            switch Monster().currentMonster {
            case "Balbox-10":
                strengthImageView.image = UIImage(named: "bird_fig1")
            case "Cuddlefish":
                strengthImageView.image = UIImage(named: "venus_fig1")
            case "Dracalope":
                strengthImageView.image = UIImage(named: "rabit_fig1")
            default:
                strengthImageView.image = UIImage(named: "bird_fig1")
            }
            

            
        let speechUtterance = AVSpeechUtterance(string: NSLocalizedString("Your monster hungers for human flesh. Begin your squats.", comment: "spoken upon loading the strength workout"))
        speechSynthesizer.speakUtterance(speechUtterance)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTime() {
        seconds += 1
        countLabel.text = NSLocalizedString("Squats: ", comment: "label for number of squats completed") + String(seconds)
        let settings = NSUserDefaults.standardUserDefaults()
        if settings.boolForKey("SFXOn") {

        let speechUtterance = AVSpeechUtterance(string: String(seconds))
        speechSynthesizer.speakUtterance(speechUtterance)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
