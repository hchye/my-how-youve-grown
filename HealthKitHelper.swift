//
//  HealthKitHelper.swift
//  MyHowYouveGrown
//
//  Created by Huan-Hua Chye on 4/25/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation
import HealthKit
import UIKit
import EventKit

class HealthKitHelper {
    
    let healthStore: HKHealthStore = HKHealthStore()
    let eventStore = EKEventStore()
    
    init() {
//        if HKHealthStore.isHealthDataAvailable() {
//        }
    }

    func authorizeHealthKit(completion: ((success:Bool, error:NSError!) -> Void)!)
    {
        // 1. Set the types you want to read from HK Store
        let healthKitTypesToRead = Set([
            HKObjectType.workoutType(),
            HKQuantityType.workoutType(),
//            HKObjectType.quantityTypeForIdentifier(HKWorkoutTypeIdentifier)
            ])
        
        // 2. Set the types you want to write to HK Store
        let healthKitTypesToWrite = Set([
            HKObjectType.workoutType(),
            HKQuantityType.workoutType(),
//            HKObjectType.quantityTypeForIdentifier(HKWorkoutTypeIdentifier)
            ])
        
        // 3. If the store is not available (for instance, iPad) return an error and don't go on.
        if !HKHealthStore.isHealthDataAvailable()
        {
            let error = NSError(domain: "healthkitaccess", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available on this device"])
            if( completion != nil )
            {
                completion(success:false, error:error)
            }
            return;
        }
        
        // 4.  Request HealthKit authorization
        healthStore.requestAuthorizationToShareTypes(healthKitTypesToWrite, readTypes: healthKitTypesToRead) { (success, error) -> Void in
            
            if( completion != nil )
            {
                completion(success:success,error:error)
            }
        }
    }
    
    func writeWorkout(startDate: NSDate, endDate: NSDate, workoutType: HKWorkoutActivityType) {
        authorizeHealthKit { (success, error) -> Void in
            let workout = HKWorkout(activityType: workoutType, startDate: startDate, endDate: endDate)
            self.healthStore.saveObject(workout) { (success: Bool, error: NSError?) -> Void in
                if success {
                    // Workout was successfully saved
                    print("Workout saved")
                }
                else {
                   //error
                    print("Error saving workout")
                }
            }
        }
    }

        func writeWorkoutWithMonster(startDate: NSDate, endDate: NSDate, workoutType: HKWorkoutActivityType, monster: String) {
            authorizeHealthKit { (success, error) -> Void in

                let workout = HKWorkout(activityType: workoutType, startDate: startDate, endDate: endDate, duration: 0, totalEnergyBurned: nil, totalDistance: nil, metadata: ["monster" : monster])

                self.healthStore.saveObject(workout) { (success: Bool, error: NSError?) -> Void in
                    if success {
                        // Workout was successfully saved
                        print("Workout saved with monster data")
//                        CalendarHelper().addEventToCalendar("Workout: " + Monster().currentMonster, startDate: startDate)
//                        self.readWorkoutsToCalendar()
                        let settings = NSUserDefaults.standardUserDefaults()
                        
                        switch monster {
                            case "Balbox-10":
                                settings.setInteger(Monster().monsterTotalWorkouts(monster) + 1, forKey: "monster1TotalWorkouts")
                                if Monster().workoutsTillNextLevel(monster) == 1 {
                                    settings.setInteger((Monster().monsterLevel(monster) + 1), forKey: "monster1Level")
                                }
                            case "Cuddlefish":
                                settings.setInteger(Monster().monsterTotalWorkouts(monster) + 1, forKey: "monster2TotalWorkouts")
                                if Monster().workoutsTillNextLevel(monster) == 1 {
                                    settings.setInteger((Monster().monsterLevel(monster) + 1), forKey: "monster2Level")
                            }
                            
                            case "Dracalope":
                                settings.setInteger(Monster().monsterTotalWorkouts(monster) + 1, forKey: "monster3TotalWorkouts")
                                if Monster().workoutsTillNextLevel(monster) == 1 {
                                    settings.setInteger((Monster().monsterLevel(monster) + 1), forKey: "monster3Level")
                            }
                            
                        default:
                            print("no monster name found")
                        }
                        
                    }
                    else {
                        //error
                        print("Error saving workout with monster data")
                    }
                }
            }
            
    }
    
    func readStretchWorkouts() {
        
        let stretchWorkout = HKQuery.predicateForWorkoutsWithWorkoutActivityType(.PreparationAndRecovery)

        let workoutType = HKWorkoutType.workoutType() as HKSampleType
        let workoutSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: true)
        
        let stretchQuery = HKSampleQuery(sampleType: workoutType, predicate: stretchWorkout, limit: HKObjectQueryNoLimit, sortDescriptors: [workoutSortDescriptor]) { (query, results, error) in
            if let _ = results {
                for result in results! {
                    let resultDescription = "Running Workout\nMonster: " + ((result.metadata?["monster"])! as! String) ?? ""
                    print("workout found: \(resultDescription) on \(result.startDate)")
                }
            }
            
        }
        
        healthStore.executeQuery(stretchQuery)
    }

    func readStrengthWorkouts() {
        
            let strengthWorkout = HKQuery.predicateForWorkoutsWithWorkoutActivityType(.FunctionalStrengthTraining)
            
            let workoutType = HKWorkoutType.workoutType() as HKSampleType
            let workoutSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: true)
            
            let strengthQuery = HKSampleQuery(sampleType: workoutType, predicate: strengthWorkout, limit: HKObjectQueryNoLimit, sortDescriptors: [workoutSortDescriptor]) { (query, results, error) in
                if let _ = results {
                    for result in results! {
                        let resultDescription = "Running Workout\nMonster: " + ((result.metadata?["monster"])! as! String) ?? ""
                        print("workout found: \(resultDescription) on \(result.startDate)")
                    }
                }
            }
            
            
            healthStore.executeQuery(strengthQuery)
    }


    
    func readCardioWorkouts() {
        
        let runningWorkout = HKQuery.predicateForWorkoutsWithWorkoutActivityType(.Running)
        
        let workoutType = HKWorkoutType.workoutType() as HKSampleType
        let workoutSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: true)
        
        let strengthQuery = HKSampleQuery(sampleType: workoutType, predicate: runningWorkout, limit: HKObjectQueryNoLimit, sortDescriptors: [workoutSortDescriptor]) { (query, results, error) in
            if let _ = results {
                for result in results! {
                    let resultDescription = "Running Workout\nMonster: " + ((result.metadata?["monster"])! as! String) ?? ""
                    print("workout found: \(resultDescription) on \(result.startDate)")
                }
            }
        }
        
        
        healthStore.executeQuery(strengthQuery)
    }

    
    func readWorkoutsToCalendar() {
        self.readStretchWorkouts()
        self.readStrengthWorkouts()
        self.readCardioWorkouts()
        
    }
}