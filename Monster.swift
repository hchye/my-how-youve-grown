//
//  MonsterStateManager.swift
//  MHYG
//
//  Created by Huan-Hua Chye on 4/29/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation

class Monster {
    var currentMonster : String
    var currentLevel : Int
    
    init() {
        let settings = NSUserDefaults.standardUserDefaults()
        currentMonster = settings.valueForKey("selectedMonster") as! String
        switch currentMonster {
            case "Balbox-10" :
                self.currentLevel = settings.integerForKey("monster1Level")

            case "Cuddlefish" :
                self.currentLevel = settings.integerForKey("monster2Level")

            case "Dracalope":
                self.currentLevel = settings.integerForKey("monster3Level")

            default:
                self.currentLevel = 1
        }
    }
    
    func monsterLevel(monster: String) -> Int {
        let settings = NSUserDefaults.standardUserDefaults()
        currentMonster = settings.valueForKey("selectedMonster") as! String
        switch currentMonster {
        case "Balbox-10" :
            return settings.integerForKey("monster1Level")
            
        case "Cuddlefish" :
            return settings.integerForKey("monster2Level")
            
        case "Dracalope":
            return settings.integerForKey("monster3Level")
            
        default:
            return 1
        }

    }
    
    
    func monsterTotalWorkouts(monster: String) -> Int {
        let settings = NSUserDefaults.standardUserDefaults()
        switch monster {
        case "Balbox-10" :
            return settings.integerForKey("monster1TotalWorkouts")
            
        case "Cuddlefish" :
            return settings.integerForKey("monster2TotalWorkouts")
            
        case "Dracalope":
            return settings.integerForKey("monster3TotalWorkouts")
            
        default:
            return 1
        }
        
    }
    
    func workoutsTillNextLevel(monster: String) -> Int {
        return 6 - (self.monsterTotalWorkouts(monster) % 6)
    }
    
    var totalWorkouts : Int {
        return self.monsterTotalWorkouts("Balbox-10") + self.monsterTotalWorkouts("Cuddlefish") + self.monsterTotalWorkouts("Dracalope")
    }
}