//
//  StartViewController.swift
//  MHYG
//
//  Created by Huan-Hua Chye on 5/2/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

       @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var subtitleTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subtitleTextView.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)

        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
