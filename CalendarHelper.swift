//
//  CalendarHelper.swift
//  MHYG
//
//  Created by Huan-Hua Chye on 4/30/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation
import EventKit
import UIKit

class CalendarHelper {

    var calendarAccess: Bool = false
    static var eventStore = EKEventStore()


    func checkCalendarAuthorizationStatus(controller: UIViewController) {
    let status = EKEventStore.authorizationStatusForEntityType(EKEntityType.Event)
        
    switch (status) {
    case EKAuthorizationStatus.NotDetermined:
        // This happens on first-run
        requestAccessToCalendar(controller)
    
        print("not determined")
    case EKAuthorizationStatus.Authorized:
        // Things are in line with being able to show the calendars in the table view
        calendarAccess = true
        print("authorized")
        
    case EKAuthorizationStatus.Restricted, EKAuthorizationStatus.Denied:
        // We need to help them give us permission
        requestAccessToCalendar(controller)
                print("denied")
        }
    }
    
    func requestAccessToCalendar(controller: UIViewController) {
        
        let settingsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SettingsViewController") as! MenuTableViewController
        
        controller.presentViewController(settingsViewController, animated: false, completion: nil)

        CalendarHelper.eventStore.requestAccessToEntityType(EKEntityType.Event, completion: {
            (accessGranted: Bool, error: NSError?) in
            
            if accessGranted == true {
                dispatch_async(dispatch_get_main_queue(), {
                    self.calendarAccess = true
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), {
                    self.calendarAccess = false
                })
            }
        })
    }
    
    func createNewCalendar(controller: UIViewController) {
        // Use Event Store to create a new calendar instance
        // Configure its title
        let newCalendar = EKCalendar(forEntityType: .Event, eventStore: CalendarHelper.eventStore)
        
        // Probably want to prevent someone from saving a calendar
        // if they don't type in a name...
        newCalendar.title = "Workout Summary"
        CalendarHelper.eventStore.requestAccessToEntityType(EKEntityType.Event,
                                             completion: {(granted: Bool, error:NSError?) in
                                                if !granted {
                                                    print("Access to store not granted")
                                                }
        })
        checkCalendarAuthorizationStatus(controller)
        
        // Access list of available sources from the Event Store

        let sourcesInEventStore = CalendarHelper.eventStore.sources
        
//         Filter the available sources and select the "Local" source to assign to the new calendar's
//         source property
        
                newCalendar.source = sourcesInEventStore.filter{
            (source: EKSource) -> Bool in
            source.sourceType.rawValue == EKSourceType.Local.rawValue
            }.first!
        
// Save the calendar using the Event Store instance
        do {
            try CalendarHelper.eventStore.saveCalendar(newCalendar, commit: true)
            NSUserDefaults.standardUserDefaults().setObject(newCalendar.calendarIdentifier, forKey: "WorkoutCalendar")
            print("saved calendar")
        } catch {
//            let alert = UIAlertController(title: "Calendar could not save", message: (error as NSError).localizedDescription, preferredStyle: .Alert)
//            let OKAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
//            alert.addAction(OKAction)

//            self.presentViewController(alert, animated: true, completion: nil)
            print("Could not save calendar")
        }
    }
    
    func addEventToCalendar(title: String, startDate: NSDate) {
        let newEvent = EKEvent(eventStore: CalendarHelper.eventStore)
        newEvent.title = title
        newEvent.startDate = startDate
    }
    
}