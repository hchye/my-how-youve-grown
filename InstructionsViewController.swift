//
//  InstructionsViewController.swift
//  MyHowYouveGrown
//
//  Created by Huan-Hua Chye on 4/25/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import UIKit

class InstructionsViewController: UIViewController {

    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet var instructionsView: UIView!
    
    @IBOutlet weak var instructionsTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "JACKPORT REGULAR NCV", size: 28)!]
        doneButton.setTitleTextAttributes([ NSFontAttributeName: UIFont(name: "JACKPORT REGULAR NCV", size: 28)!], forState: UIControlState.Normal)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
 instructionsTextView.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
