//
//  StretchViewController.swift
//  MyHowYouveGrown
//
//  Created by Huan-Hua Chye on 4/17/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import UIKit
import AVFoundation
import HealthKit

class StretchViewController: UIViewController {

    var seconds = 0
    var stretchTimer: Int = 30
    var timer = NSTimer()
    var timerRunning = false
    let speechSynthesizer = AVSpeechSynthesizer()
    var stretchList: [[String: String]] = [["":""]]
    var stretchCount = 0
    let stretchDuration = 15
    var startTime: NSDate? = nil
    var stopTime: NSDate? = nil
    
    @IBOutlet weak var homeButton: UIBarButtonItem!
    
    @IBOutlet weak var stretchImage: UIImageView!
    
    @IBOutlet weak var startStopButton: UIButton!
    
    @IBOutlet weak var exerciseName: UILabel!
    
    @IBOutlet weak var stretchDescriptionLabel: UITextView!
    
    
    @IBOutlet weak var counterLabel: UILabel!
    
    @IBAction func startStopButtonPressed(sender: UIButton) {
        if timerRunning {
            //stop
            startStopButton.setTitle(NSLocalizedString("Start!", comment: "command, label for button; when pressed, the count for stretches starts"), forState: UIControlState.Normal)
            timer.invalidate()
            timerRunning = false

            let settings = NSUserDefaults.standardUserDefaults()
            if settings.boolForKey("SFXOn") {
            speechSynthesizer.stopSpeakingAtBoundary(AVSpeechBoundary.Word)
            
            let speechUtterance = AVSpeechUtterance(string: NSLocalizedString("Great workout. Your monster has been appeased.", comment: "phrase spoken upon finishing workout"))
            speechSynthesizer.speakUtterance(speechUtterance)
            }
            
            stopTime = NSDate()
            let monster = settings.valueForKey("selectedMonster") as? String ?? ""

            HealthKitHelper().writeWorkoutWithMonster(startTime!, endDate: stopTime!, workoutType: HKWorkoutActivityType.PreparationAndRecovery, monster: monster)
        } else {
            //start
            startStopButton.setTitle(NSLocalizedString("Stop!", comment: "command, label for button; when pressed, the count for stretches stops"), forState: UIControlState.Normal)
            counterLabel?.text = NSLocalizedString("Seconds Remaining: ", comment: "Label for seconds remaining in the current stretch") + String(stretchTimer)
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(StretchViewController.addTime), userInfo: nil, repeats: true)
            timerRunning = true
            startTime = NSDate()
        }

        
    }
    
    func addTime() {
        seconds += 1
        stretchTimer -= 1
        counterLabel?.text = NSLocalizedString("Seconds Remaining: ", comment: "Label for seconds remaining in the current stretch") + String(stretchTimer)
        let totalExerciseTime = stretchList.count * stretchDuration
        if seconds < totalExerciseTime {
            
            if (seconds % stretchDuration == 0)  {
            //read description for new stretch
                newStretchLoaded(seconds / stretchDuration)
            }
            else if (seconds == 1)
            {
                //read description for first stretch in series
                newStretchLoaded(0)
        }
        }
        else
            //if workout is completed
        {

            let settings = NSUserDefaults.standardUserDefaults()
            if settings.boolForKey("SFXOn") {

            let speechUtterance = AVSpeechUtterance(string: NSLocalizedString("You have completed all available stretches. Your monster now revels in the glorious suppleness of your flesh.", comment: "spoken when stretch workout is completed, i.e. you've finished all the stretches"))
            speechSynthesizer.speakUtterance(speechUtterance)
            
            }
            //MARK: TODO: save total amount of time worked out
            seconds = 0
            stretchTimer = stretchDuration
            timer.invalidate()
            startStopButton.setTitle(NSLocalizedString("Start!", comment: "command, label for button; when pressed, the count for stretches starts"), forState: UIControlState.Normal)
            counterLabel?.text = NSLocalizedString("Seconds Remaining: ", comment: "Label for seconds remaining in the current stretch") + String(stretchTimer)
            let stretchNumber = seconds / stretchDuration
            let currentStretch = stretchList[stretchNumber]
            exerciseName?.text = currentStretch["stretchName"]
        }
    }


    func newStretchLoaded(stretchNumber: Int) {
        //switch once each stretch is done
        
        stretchTimer = stretchDuration
        counterLabel?.text = NSLocalizedString("Seconds Remaining: ", comment: "Label for seconds remaining in the current stretch") + String(stretchTimer)
        let currentStretch = stretchList[stretchNumber]
        let currentStretchName = currentStretch["stretchName"] ?? ""
        exerciseName?.text = currentStretchName
        let currentStretchDescription = currentStretch["stretchDescription"] ?? ""
        //set description

        stretchDescriptionLabel.text = currentStretchDescription
        
        let settings = NSUserDefaults.standardUserDefaults()
        if settings.boolForKey("SFXOn") {

        //read the name and description out loud
        let speechUtterance = AVSpeechUtterance(string: currentStretchName + ". " + currentStretchDescription)
        speechSynthesizer.speakUtterance(speechUtterance)
        }
        //switch the image
        stretchImage.image = UIImage(named: currentStretchName)
        
    }
    
    @IBAction func resetWorkoutButtonPressed(sender: UIButton) {
        seconds = 0
        stretchTimer = stretchDuration
        timer.invalidate()
        startStopButton.setTitle(NSLocalizedString("Start!", comment: "command, label for button; when pressed, the count for stretches starts"), forState: UIControlState.Normal)
        counterLabel?.text = NSLocalizedString("Seconds Remaining: ", comment: "Label for seconds remaining in the current stretch") + String(stretchTimer)
        let stretchNumber = seconds / stretchDuration
        let currentStretch = stretchList[stretchNumber]
        exerciseName?.text = currentStretch["stretchName"]
        //MARK: TODO: save total amount of time worked out
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settings = NSUserDefaults.standardUserDefaults()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
        stretchDescriptionLabel!.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "JACKPORT REGULAR NCV", size: 28)!]
        homeButton.setTitleTextAttributes([ NSFontAttributeName: UIFont(name: "JACKPORT REGULAR NCV", size: 28)!], forState: UIControlState.Normal)
        exerciseName.font = UIFont(name: "JACKPORT REGULAR NCV", size: 28)!
    
        
        if settings.boolForKey("musicOn") {
            MusicHelper.sharedHelper.playBackgroundMusic("Chariots Of The Elder Gods")
        } else {
            MusicHelper.sharedHelper.stopBackgroundMusic()
        }

        if let path = NSBundle.mainBundle().pathForResource("stretches", ofType: "json")
        {
            if let jsonData = NSData(contentsOfFile: path)
            {
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(jsonData, options: .AllowFragments)
                    
                    if let stretches = json["stretches"] as? [[String: String]] {
                        
//                        for stretch in stretches {
//                            print(stretch)
//                        }
                        stretchList = stretches
                        exerciseName?.text = stretchList[0]["stretchName"]
                    }
                } catch {
                    print("error serializing JSON: \(error)")
                }
            }
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation
v
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
