//
//  statsTableViewCell.swift
//  MHYG
//
//  Created by Huan-Hua Chye on 5/9/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import UIKit

class statsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
