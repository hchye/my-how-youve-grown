//
//  SelectWorkoutTableViewController.swift
//  MyHowYouveGrown
//
//  Created by Huan-Hua Chye on 4/25/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import UIKit

class SelectWorkoutTableViewController: UITableViewController {

    @IBAction func cancelButtonTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    
    @IBOutlet weak var stretchLabel: UITableViewCell!
    
    @IBOutlet weak var cardioLabel: UITableViewCell!
    
    @IBOutlet weak var strengthLabel: UITableViewCell!
    
    override func viewDidLoad() {

        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
        stretchLabel.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
        cardioLabel.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)
        strengthLabel.backgroundColor = UIColor(patternImage: UIImage(named: "app background.jpg")!)

        cancelButton.setTitleTextAttributes([ NSFontAttributeName: UIFont(name: "JACKPORT REGULAR NCV", size: 20)!], forState: UIControlState.Normal)
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "JACKPORT REGULAR NCV", size: 20)!]
        
//music on this screen is annoying, turning it off
        //        let settings = NSUserDefaults.standardUserDefaults()
//        if settings.boolForKey("musicOn") {
//            MusicHelper.sharedHelper.playBackgroundMusic("Monster Theme")
//        } else {
//            MusicHelper.sharedHelper.stopBackgroundMusic()
//        }
   

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
